package br.com.osworks.exception;

public class ClienteException extends RuntimeException {

    public ClienteException(String mensagem){
        super(mensagem);
    }

}
