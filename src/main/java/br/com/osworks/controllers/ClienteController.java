package br.com.osworks.controllers;

import br.com.osworks.models.Cliente;
import br.com.osworks.repositories.ClienteRepositories;
import br.com.osworks.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteRepositories clienteRepositories;

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    public List<Cliente> clientes(){
        return clienteRepositories.findAll();
    }

    @GetMapping("/{clienteId}")
    public ResponseEntity<Cliente> buscar(@PathVariable Long id){
        Optional<Cliente> cliente = clienteRepositories.findById(id);

        if(cliente.isPresent()){
           return ResponseEntity.ok(cliente.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private Cliente adicionar(@Valid @RequestBody Cliente cliente){
        return clienteService.salvar(cliente);
    }

    @PutMapping("/{clienteId}")
    private ResponseEntity<Cliente> atualizar(@Valid @PathVariable Long id, @RequestBody Cliente cliente){
        if(!clienteRepositories.existsById(id)){
            return ResponseEntity.notFound().build();
        }
        cliente.setId(id);
        clienteService.salvar(cliente);

        return ResponseEntity.ok(cliente);
    }

    @DeleteMapping("/{clienteId}")
    public ResponseEntity<Void> remover(@PathVariable Long id){
        if(!clienteRepositories.existsById(id)){
            return ResponseEntity.notFound().build();
        }
        clienteService.excluir(id);

        return ResponseEntity.noContent().build();
    }
}
